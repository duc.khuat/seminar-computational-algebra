##
# p,q are polynomials over the same ring.
# p[i] is the i-1th degree coefficient.
polymult := function(p,q,u,v)
    local res, i,j,n,m;
    n := Size(p); # this is deg(p) + 1
    m := Size(q); # this is deg(q) + 1
    res := List( [1..Minimum(n+m-1,u)], x -> 0);
    for i in [1..n] do
        for j in [1..m] do
            if i+j-1 <= u then
                res[i+j-1] := res [i+j-1] + p[i]*q[j];
            else
                res[i+j-1-u] := res [i+j-1-u] + v*p[i]*q[j];
            fi;
        od;
    od;
    return res;
end;;
##
forth_transform := function (p,u,v)
    local pmod1, pmod2,i;
    pmod1 := [];
    pmod2 := [];
    for i in [u+1.. Size(p)] do
        pmod1[i-u] := p[i-u] + v*p[i];
    od;
    for i in [u+1.. Size(p)] do
        pmod2[i-u] := p[i-u] - v*p[i];
    od;
    return [pmod1, pmod2];
end;;

##

back_transform := function (pq,u,v)
    local res,i,diff;
    res := [];
    for i in [1..Size(pq[1])] do
        diff  := (pq[1][i]-pq[2][i])/2;
        res[i] := pq[1][i] - diff;
        res[i+u] := diff/v;
    od;
    return res;
end;;

##

NTT := function(p,q,u,v)
    local p1p2,q1q2,res_trans, res;
    res := [];
    res_trans:=[];
    p1p2 := forth_transform(p,u,v);
    q1q2 := forth_transform(q,u,v);
    res_trans[1] := polymult(p1p2[1],q1q2[1],u,v);
    res_trans[2] := polymult(p1p2[2],q1q2[2],u,-v);
    res := back_transform(res_trans,u,v);
    return res;
end;;


##
#Functions for testing.
##
invertRep := function (p,x)
local i,f;
    f := 0;
    for i in [1..Size(p)] do
        f := f + p[i]*x^(i-1);
    od;
    return f;
end;;
##
Testfunc := function(p,q,u,v,x)
local f,h,i,res;
    res := NTT(p,q,u,v);
    f:= invertRep(p,x);
    h:= invertRep(q,x);
    return ((f*h) mod (x^(2*u)-v^2)) = invertRep(res,x);
end;



