##
Swap:= function (A,i,j)
local res,r;
    res :=A;
    r := A[i];
    res[i] := A[j];
    res[j] := r;
    return res;
end;;

