##
# Tests
###

B :=[[1.,0.,2.],[1.,-1.,2.],[1.,0.,1.]];
Print("Example Matrix B= \n");
Display(B);
Bnew := sizereduction(B1);
C:= GramSchmidt(B1);
C2:= GramSchmidt(Bnew);
Print("Testing Gram-Schmidt... \n We obtain the orthogonal basis \n B^* = \n");
Display(C2[1]);
Print(" Now we test if Q*B gives B^* \n");
Display (C[1]*B = C[2]);
Print("Test one successful! \n");

##

Print("Test two! \n We will test the LLL-Algorithm.\n We obtain the LLL-reduced-basis \n L= \n ");
L := LLLAlgorithm(B,3/4);
Display(L);
Print("We test if L spans the same lattice:\n");
Print("The matrix B^(-1) *L  must be an integer matrix \n");
Display(B^(-1)*L);
Print("It is! So L and B span the same lattice.\n");
Print("Also L is indeed orthogonal. The algorithm was successful here.\n");

##

Print("Perfomance Test! \n");


