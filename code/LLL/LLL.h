
##
#Given a matrix B we compute a orthogonal basis with respect to the rows of B by applying the Gram-Schmidt Algorithm.
#Q*B = Bnew
#Q is a lower triangular matrix.
# input:    A invertible matrix B.
# return:    unitary lower triangular matrix Q with Q*B = Bnew. The rows of Bnew span an orthogonal basis.
GramSchmidt := function (B)
local n,m,Q, Bnew,mu_ij, i,j,b;
    n := Size(B);
    m := Size(B[1]);
    Bnew := NullMat(n,m);
    Bnew[1] := B[1];
    Q:= IdentityMat(n);
    for i in [2..n] do
        b :=B[i];
        for j in [1..i-1] do
            mu_ij := (b*Bnew[j])/(Bnew[j]* Bnew[j]);
            b := b - mu_ij * Bnew[j];
            Q[i][j] := -mu_ij;
        od;
        Bnew[i] := b;
    od;
    return [Q,Bnew];
end;;

##
sizereduction := function (B)
local n,m,i,j,Bnew,A, mu, mu0;
    n := Size(B);
    m := Size(B[1]);
    A := GramSchmidt(B);
    Bnew:= NullMat(n,m);
    for i in [1..n] do
        Bnew[i] := B[i];
        for j in [1..(i-1)] do
            mu0 := Round(Float(A[1][i][j]));
            Bnew[i] := Bnew[i] + mu0*A[2][j];
        od;
    od;
    return Bnew;
end;;

##
# usually d = 3/4 and in general 1/2 < d < 1
LLLAlgorithm:= function(B, d)
local n,k,A,Q,j, Bl;
    n := Size(B);
    Bnew := sizereduction(B);
    A := GramSchmidt(Bnew);
    Q:= A[1];
    Bl := [];
    for  i in [1..n] do
        Add(Bl, A[2][i] * A[2][i]);
    od;
    k := 2;
    while k <= n do
        if Bl[k] < (d-Q[k][k-1]^2)*Bl[k-1] then
            Bnew := Swap(Bnew, k, k-1);
            Bl := Swap(Bl,k,k-1);
            Bnew := sizereduction(Bnew);
            A := GramSchmidt(Bnew);
            Q:= A[1];
            k := Maximum(2,k-1);
        else
            k:= k+1;
        fi;
    od;
    return Bnew;
end;;


